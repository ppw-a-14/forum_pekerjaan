# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth import logout as auth_logout
from social_django.models import UserSocialAuth
from .models import Company, Post, Comment
from .forms import PostForm
response = {}

def landing(request):
    post = Post.objects.all().order_by('-id')
    if post.count() > 0:
        response['posts'] = post
        
    return render(request, 'landing.html', response)

def logout(request):
    auth_logout(request)
    return redirect('/')

@login_required(login_url='/')
def profile(request):
	html = 'company/profile.html'
	return render(request, html, response)

@login_required(login_url='/')
def forum_lowongan(request):
    # get company object
    admin_id = request.user.id
    try:
        company = Company.objects.get(admin_id=admin_id)
    except Company.DoesNotExist:
        return HttpResponseRedirect(reverse('company:profile'))

    form = PostForm(request.POST or None)
    
    if request.method == 'POST' and form.is_valid():
        # create new post
        post = Post()
        post.lowongan_kerja = request.POST['post_lowongan']
        post.company = company
        post.save()

        return HttpResponseRedirect(reverse('company:forum_lowongan'))
    else:
        # get all post
        post_list = Post.objects.filter(company=company).order_by('-id')

        # create pagination
        paginator = Paginator(post_list, 5) # show 5 posts per page
        page = request.GET.get('page')
        try:
            posts = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            posts = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            posts = paginator.page(paginator.num_pages)

        response['posts'] = posts
        response['post_form'] = PostForm

        html = 'company/forum.html'
        return render(request, html, response)
