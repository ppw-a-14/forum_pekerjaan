# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Company(models.Model):
	admin_id = models.IntegerField();
	name = models.CharField(default="", max_length=250)
	logo = models.CharField(default="", max_length=1000)
	tipe = models.CharField(max_length=400)
	web = models.CharField(max_length=1000)
	spesiality = models.CharField(max_length= 900)

class Post(models.Model):
	company = models.ForeignKey(Company)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	lowongan_kerja = models.CharField(max_length=1000)

class Comment(models.Model):
	post = models.ForeignKey(Post)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	diskusi_lowongan_kerja = models.CharField(max_length=1000)
