# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import resolve
from social_django.models import UserSocialAuth
from .views import forum_lowongan
from .models import Company, Post, Comment

def mock_login(self, company=True):
    # create dummy linkedin user
    self.user = User.objects.create(username='test_username', password='test_password', is_active=True, is_staff=True, is_superuser=True)
    self.user.set_password('test_password') 
    self.user.save() 
    self.user = authenticate(username='test_username', password='test_password') 
    login = self.client.login(username='test_username', password='test_password') 
    self.assertTrue(login)

    if company:
        # create dummy linkedin company
        company = Company.objects.create(admin_id=self.user.id)

# Create your tests here.
class ForumPekerjaanUnitTest(TestCase):

    def test_landing_page(self):
        # create dummy post
        mock_login(self)
        company = Company.objects.filter(admin_id=self.user.id).first()
        post = Post.objects.create(company=company, lowongan_kerja='dummy_post')

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_logout(self):
        mock_login(self)

        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_profile(self):
        mock_login(self)

        response = self.client.get('/company/profile/')
        self.assertEqual(response.status_code, 200)

    def test_forum_lowongan_before_login(self):
        response = self.client.get('/company/forum/')
        self.assertEqual(response.status_code, 302)
    
    def test_forum_lowongan_after_login_with_company(self):
        # login
        mock_login(self)

        # default
        response = self.client.get('/company/forum/')
        self.assertEqual(response.status_code, 200)

        # empty page
        response = self.client.get('/company/forum/?page=322')
        self.assertEqual(response.status_code, 200)

    def test_forum_lowongan_after_login_without_company(self):
        # login
        mock_login(self, company=False)

        response = self.client.get('/company/forum/')
        self.assertEqual(response.status_code, 302)

    def test_forum_lowongan_add_new_post(self):
        # login
        mock_login(self)

        post_data = {'post_lowongan': 'Dibutuhkan pekerja'}
        response = self.client.post('/company/forum/', post_data)
        self.assertEqual(response.status_code, 302)

        counting_all_post = Post.objects.all().count()
        self.assertEqual(counting_all_post, 1)