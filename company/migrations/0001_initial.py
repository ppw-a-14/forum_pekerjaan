# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-12 12:12
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('admin_id', models.IntegerField()),
                ('tipe', models.CharField(max_length=400)),
                ('web', models.CharField(max_length=250)),
                ('spesiality', models.CharField(max_length=900)),
            ],
        ),
    ]
