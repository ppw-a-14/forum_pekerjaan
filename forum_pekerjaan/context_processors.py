from company.models import Company
from .utils import get_and_set_company
from ast import literal_eval

def company(request):
    ctx = {}
    if not request.user.is_anonymous:
        try:
            company = Company.objects.get(admin_id=request.user.id)
            ctx['company'] = {
                'name': company.name,
                'logo_url': company.logo,
                'type': company.tipe,
                'web_url': company.web,
                'speciality': company.spesiality,
            }
        
        except Company.DoesNotExist:
            company_setted = get_and_set_company(request.user.id)
            if company_setted:
                company = Company.objects.get(admin_id=request.user.id)
                ctx['company'] = {
                    'name': company.name,
                    'logo_url': company.logo,
                    'type': company.tipe,
                    'web_url': company.web,
                    'speciality': company.spesiality,
                }
    return ctx