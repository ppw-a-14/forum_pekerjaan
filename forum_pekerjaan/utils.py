from social_django.models import UserSocialAuth
from company.models import Company
import requests

def get_and_set_company(userId):
    social_model = UserSocialAuth.objects.get(id=userId)
    access_token = social_model.extra_data['access_token']
    target_url = "https://api.linkedin.com/v1/companies?format=json&is-company-admin=true"

    response = requests.get(
        target_url,
        params={'oauth2_access_token': access_token}
    )

    responseJson = response.json()
    if response.status_code == 200 and responseJson['_total'] > 0:
        companyId = responseJson['values'][0]['id']

        response = requests.get(
            "https://api.linkedin.com/v1/companies/" + str(companyId) + ":(name,description,logo-url,company-type,website-url,specialties)?format=json",
            params={'oauth2_access_token': access_token},
        )

        if response.status_code == 200:
            responseJson = response.json()
            company = Company.objects.create(
                admin_id=userId,
                name=responseJson['name'],
                logo=responseJson['logoUrl'],
                tipe=responseJson['companyType']['name'],
                web=responseJson['websiteUrl'],
                spesiality=responseJson['specialties']['values'],
            )
            company.save()
            return True
        else:
            return False
    return False