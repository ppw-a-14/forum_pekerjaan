[![pipeline status](https://gitlab.com/ppw-a-14/forum_pekerjaan/badges/master/pipeline.svg)](https://gitlab.com/ppw-a-14/forum_pekerjaan/commits/master)
[![coverage report](https://gitlab.com/ppw-a-14/forum_pekerjaan/badges/master/coverage.svg)](https://gitlab.com/ppw-a-14/forum_pekerjaan/commits/master)

# [Lin-kin](http://lin-kin.herokuapp.com/)

Lin-kin is a social media powered by Django Framework.

### Collaborators
  - Asel Hidayat Sjamhars (1606839006)
  - Endrawan Andika Wicaksana (1606862740)
  - Ignatius Rahardi Prasojo (1606875951)

### Installation
Lin-kin requires [Python](https://www.python.org/) v2.7, v3.4, or v3.5 and [Pip](https://pip.pypa.io/) to run.

Install the dependencies and start the server.

```sh
$ pip install -r requirements.txt
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py runserver
```
